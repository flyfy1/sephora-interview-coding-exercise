# Sephora Interview Coding Exercise

## Setup

```bash
# after checkout the project, in root folder
bundle install
bundle exec rake db:setup

# to load mock data
bundle exec rake mock:product

# to start server
bundle exec rails s

# to run test
bundle exec rspec 
```

## API documentation

I've used API-pie for API docs, one can see the detail documentation here: 
<http://localhost:3000/apipie>.

Note that response is not documented in proper way. A better choice for
documenting API is to use swagger: <http://swagger.io/specification/>.

The resposne is standard, following [JSON API Spec](http://jsonapi.org).

## Admin UI

Admin UI can be visited via ActiveAdmin: <http://localhost:3000/admin/>

Username & password are defined in the seed file.
