class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :sold_out, :category, :under_sale, :price, :sale_price, :price_display

  def price_display
    format('%0.2f', object.price.to_f / 100)
  end
end
