class Product < ApplicationRecord
  validates :price, numericality: { greater_than: 0 }
  validates :sale_price, numericality: { greater_than: 0 }, if: :under_sale
  validates :sale_price, numericality: { less_than: :price }, if: :under_sale

  validates :name, presence: true

  before_validation :set_defaults

  private

  def set_defaults
    self.under_sale = false if under_sale.nil?
    self.sold_out = false if sold_out.nil?
  end
end
