class ProductsController < ApplicationController
  api :GET, '/products', 'Get details about a product'
  param :q, Hash, 'The query object following ransack spec. Examples are' do
    param :price_gt, :number, desc: 'price greater than a number, where price is in unit of cent'
    param :price_lt, :number, desc: 'price less than a number, where price is in unit of cent'
    param :category_eq, String, desc: 'category equals to a string'
    param :s, String, desc: 'sorting. for example, `price desc`'
  end
  param :page, Hash, desc: 'Page info' do
    param :number, :number, desc: 'the page number'
    param :size, :number, desc: 'number of elements per page'
  end
  def index
    @products = Product.ransack(params['q']).result
                       .page(page_param['number'] || 1)
                       .per(page_param['size'] || 25)

    render json: @products # if stale?(last_modified: @products.pluck(:updated_at).max)
  end

  api :GET, '/products/:id', 'Get details about a product'
  param :id, :number, desc: 'ID of the product', required: true
  def show
    @product = Product.find params[:id]

    render json: @product if stale?(last_modified: @product.updated_at)
  end

  private

  def page_param
    @page_param ||= { 'size' => 25, 'number' => 1 }.merge((params[:page].try(:to_unsafe_hash) || {}))
  end
end
