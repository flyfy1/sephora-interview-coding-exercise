ActiveAdmin.register Product do
  permit_params :name, :sold_out, :category, :under_sale, :price, :sale_price
end
