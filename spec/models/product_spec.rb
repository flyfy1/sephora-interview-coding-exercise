require 'rails_helper'

RSpec.describe Product, type: :model do
  let(:product) { build(:product) }
  let(:product_in_sale) { build(:product, :in_sale) }
  let(:product_sold_out) { build(:product, :sold_out) }

  describe 'valid' do
    it { expect(product).to be_valid }
    it { expect(product_in_sale).to be_valid }
    it { expect(product_sold_out).to be_valid }
    it { expect(build(:product, :sold_out, :in_sale)).to be_valid }
  end

  describe 'invalid' do
    context 'negative price' do
      before { product.price = -10 }
      it { expect(product).not_to be_valid }
    end

    context 'negative sale price' do
      before { product_in_sale.sale_price = -10 }
      it { expect(product_in_sale).not_to be_valid }
    end

    context 'sale price more expensive' do
      before { product_in_sale.sale_price = 2000 }
      it { expect(product_in_sale).not_to be_valid }
    end

    context 'name' do
      before { product.name = nil }
      it { expect(product).not_to be_valid }
    end
  end

  describe 'initialization - sold_out / under_sale' do
    before do
      product.sold_out = nil
      product.under_sale = nil
      product.save!
      product.reload
    end

    it { expect(product.sold_out).to be false }
    it { expect(product.under_sale).to be false }
  end
end
