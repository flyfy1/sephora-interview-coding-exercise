FactoryGirl.define do
  factory :product do
    name 'Mock Product'
    sold_out false
    under_sale false
    price 1199
    sale_price nil
    category 'unknown'

    trait :in_sale do
      under_sale true
      sale_price 1099
    end

    trait :sold_out do
      sold_out true
    end
  end
end
