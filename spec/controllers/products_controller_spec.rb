require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  let!(:product) { create(:product) }
  let!(:product_in_sale) { create(:product, :in_sale) }
  let!(:product_sold_out) { create(:product, :sold_out) }

  let(:data) { json['data'] }
  let(:attributes) { data['attributes'] }

  before { @request.env['Content-Type'] = 'application/vnd.api+json' }

  describe '#show' do
    before { get :show, params: { id: product.id } }
    it { expect(data['id']).to eq product.id.to_s }
    it { expect(data['type']).to eq 'products' }
    it { expect(attributes['name']).to eq product.name }
    it { expect(attributes['sold-out']).to eq product.sold_out }
    it { expect(attributes['price']).to eq product.price }
    it { expect(attributes['price-display']).to eq '11.99' }
    it { expect(attributes['under-sale']).to eq product.under_sale }
  end

  describe '#index' do
    context 'count' do
      before { get :index }
      it { expect(json['data'].length).to eq 3 }
    end

    context 'pagination' do
      let!(:more_products) { create_list(:product, 45) }

      describe 'page number' do
        before { get :index, params: { page: { number: 2 } } }
        it { expect(URI.unescape(json['links']['self'])).to include('page[number]=2&page[size]=25') }
        it { expect(URI.unescape(json['links']['prev'])).to include('page[number]=1&page[size]=25') }
        it { expect(URI.unescape(json['links']['first'])).to include('page[number]=1&page[size]=25') }
      end

      describe 'page size' do
        before { get :index, params: { page: { size: 10 } } }
        it { expect(URI.unescape(json['links']['self'])).to include('page[number]=1&page[size]=10') }
        it { expect(URI.unescape(json['links']['next'])).to include('page[number]=2&page[size]=10') }
        it { expect(data.length).to be 10 }
      end
    end

    context 'conditional' do
      before do
        create(:product, price: 100)
        create(:product, price: 200)
        create(:product, price: 300, category: 'toys')
        create_list(:product, 25)
      end

      context 'filter' do
        before { get :index, params: { q: { price_lt: 250 } } }
        it { expect(data.length).to eq 2 }
      end

      context 'filter - category' do
        before { get :index, params: { q: { price_gt: 150, category_eq: 'unknown' }, page: { number: 2 } } }

        it { expect(json.length).to eq 2 }
        it { expect(URI.unescape(json['links']['self'])).to include('page[number]=2&page[size]=25') }
      end

      context 'sorting with filter' do
        context 'asc' do
          before { get :index, params: { q: { price_lt: 350 }, s: 'price' } }
          it { expect(data.map { |o| o['attributes']['price'] }).to eq [100, 200, 300] }
        end

        context 'desc' do
          before { get :index, params: { q: { price_lt: 350, s: 'price desc' } } }
          it { expect(data.map { |o| o['attributes']['price'] }).to eq [300, 200, 100] }
        end
      end
    end
  end
end
