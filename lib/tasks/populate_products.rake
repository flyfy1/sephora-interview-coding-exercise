namespace :mock do
  desc 'Populate mock products'
  task product: :environment do
    require 'faker'

    r = Random.new

    update_sale_info = lambda do |config, in_sale_prob: 0.4, sold_out_prob: 0.1|
      if r.rand < in_sale_prob
        config[:under_sale] = true
        config[:sale_price] = config[:price] * 0.8
      end

      config[:sold_out] = r.rand < sold_out_prob
    end

    30.times do |i|
      product_config = { name: Faker::Beer.name, price: r.rand(200..500), category: 'beer' }
      update_sale_info[product_config]
      Product.create_with(product_config).find_or_create_by! id: i + 1
    end

    40.times do |i|
      product_config = { name: Faker::Book.title, price: r.rand(2000..10_000), category: 'book' }
      update_sale_info[product_config, in_sale_prob: 0.5]
      Product.create_with(product_config).find_or_create_by! id: i + 31
    end

    20.times do |i|
      product_config = { name: Faker::Cat.name, price: r.rand(500..10_000), category: 'cat' }
      update_sale_info[product_config, in_sale_prob: 0.2]
      Product.create_with(product_config).find_or_create_by! id: i + 71
    end

    ActiveRecord::Base.connection.execute "SELECT setval('products_id_seq', (SELECT max(id) FROM products));"
  end
end
