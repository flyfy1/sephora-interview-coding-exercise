class AddIndexOnProduct < ActiveRecord::Migration[5.0]
  def change
    add_index :products, :price
    add_index :products, :sale_price
    add_index :products, :category
  end
end
