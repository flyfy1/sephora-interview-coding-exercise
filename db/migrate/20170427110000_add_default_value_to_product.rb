class AddDefaultValueToProduct < ActiveRecord::Migration[5.0]
  def up
    change_column :products, :sold_out, :boolean, default: false, null: false
    change_column :products, :under_sale, :boolean, default: false, null: false
  end

  def down
    change_column :products, :sold_out, :boolean, default: nil, null: true
    change_column :products, :under_sale, :boolean, default: nil, null: true
  end
end
